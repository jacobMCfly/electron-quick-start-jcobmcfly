# electron-quick-start-and-compilation 

  fuente 
  
  https://www.youtube.com/watch?v=GEXhlGH7ylE
 
A basic Electron application needs just these files:

- `package.json` - Points to the app's main file and lists its details and dependencies.
- `main.js` - Starts the app and creates a browser window to render HTML. This is the app's **main process**.
- `index.html` - A web page to render. This is the app's **renderer process**.

 

```bash
# Clone this repository original without modifications by jacobMCfly
git clone https://github.com/electron/electron-quick-start
# Go into the repository
cd electron-quick-start
# Install dependencies
npm install
# Run the app
npm start
```
 
 tambien hay que instalar el compilador : 
 
 ```bash
# npm install electron-builder --save-dev
```

## para compilar en sistema operativo 

- windows : icon.ico
- Ios & Linux : background.png
- se necesita un archivo icon.icns en ambos

 hay que agregar al Json : 

```json
 "scripts": {
    "start": "electron .",
    "pack" : "build --dir",
    "dist" : "build"
  }
  ```

author y licencia es un elemento requerido
```json
"author": "jacobMCfly",
  "license": "MIT",
  "devDependencies": {
    "electron": "^4.0.4",
    "electron-builder": "^20.38.5"
  }
```

Agregar al json debajo de lisence la llava build donde se agregan las opciones de compilación para cada sistema operativo
```json
 "build": {
    "appId": "aqui-va-apple-id",
    "asar": true,
    "dmg": {
      "contents": [
        {
          "x": 110,
          "y": 150
        },
        {
          "x": 240,
          "y": 150,
          "type": "link",
          "path": "/Applications"
        }
      ]
    },
    "linux": {
      "target": [
        "AppImage",
        "deb"
      ]
    },
    "win": {
      "target": "NSIS",
      "icon": "build/icon.ico"
    }
  }
```
En caso de ser utilizado con electrón estas dependencias son necesarias

```json
  "dependencies": {
    "request": "*",
    "request-primise" :"*"
  }
  ```
  
